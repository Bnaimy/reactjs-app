import React, { Component } from 'react';
import RightMenu from './RightMenu'
import { Drawer, Button } from 'antd';
import { Menu} from 'antd';
import mylogo from '../../images/mylogo.png';
import home from '../../images/home.svg';
import Pdf from "../../documents/CV_NAIMY_BOUCHAIB.pdf";
import FormDialog from '../popup/popup.js';
const add = "#" ;
class Navbar extends Component {
	onResumeClick() {
    window.open(Pdf);
  }
	state = {
    current: 'mail',
    visible: false
  }
  showDrawer = () => {
    this.setState({
      visible: true,
    });
  };

  onClose = () => {
    this.setState({
      visible: false,
    });
	};
  render() {
    return (
        <nav className="menuBar">
				<div className="menuCon">
				<a href={add}><img src={mylogo} alt="not show" className="mylogo" /></a>
				<div className="rightMenu">
	        			<RightMenu />
								</div>
								</div>
				    <Button className="barsMenu" type="default" onClick={this.showDrawer}>
		          <span><img src={home} alt="not show" className="home"/></span>
		        </Button>
				    <Drawer
		          title="MENU"
		          placement="left"
		          closable={true}
		          onClose={this.onClose}
		          visible={this.state.visible}
		        >
						<br />
						<Menu mode="vertical" className="Menu">
						<Menu.Item key="mail">
						<FormDialog />
						</Menu.Item>
							<br />
							<br />
							<Menu.Item key="app">
							<Button variant="outlined" color="primary" onClick={this.onResumeClick}>RESUME</Button>
							</Menu.Item>
							</Menu>
		        </Drawer>
        </nav>
    );
  }
}

export default Navbar;