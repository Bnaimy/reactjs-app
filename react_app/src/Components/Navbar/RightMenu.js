import React, { Component } from 'react';
import { Menu} from 'antd';
import Button from '@material-ui/core/Button';
import Pdf from "../../documents/CV_NAIMY_BOUCHAIB.pdf";
import FormDialog from '../popup/popup.js';
class RightMenu extends Component {
  onResumeClick() {
    window.open(Pdf);
  }
  render() {
    return (
			<Menu mode="horizontal" className="Menu">
        <Menu.Item key="mail">
        <FormDialog />
        </Menu.Item>
        <Menu.Item key="app">
        <Button variant="outlined" color="primary" onClick={this.onResumeClick}>resume</Button>
        </Menu.Item>
      </Menu>
    );
  }
}

export default RightMenu;
