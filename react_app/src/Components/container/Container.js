import React , { Component }  from 'react';
class About extends Component {
  render() {
    let style = {
      color:"white",
      padding:100
    };
  return (
    <div className="about">
    <h1 className="t" style={style}>Hi, I’m Choaib. Nice to meet you.</h1>
    <p>Since beginning my journey as a freelance designer nearly 8 years ago, I've done remote work for agencies, consulted for startups, and collaborated with talented people to create digital products for both business and consumer use. I'm quietly confident, naturally curious, and perpetually working on improving my chops one design problem at a time.</p>
    </div>
  );
}
}
export default About ;