import React, { Component } from 'react';
import '../../css/App.css';
import Paper from '@material-ui/core/Paper';
import logo from '../../images/developper.svg';
import hero from '../../images/hero.png';
import Navbar from '../Navbar';
import Container from '../container/Container.js';
import Grid from '../grids/Grids.js';
import Recent from '../recentwork/recentworks.js';
import '../../styles.scss';
import Wobble from 'react-reveal/Wobble';
import Zoom from "react-reveal/Zoom";
import Fade from 'react-reveal/Fade';
import config from 'react-reveal/globals';
import '../../css/App.css' ;
config({ ssrFadeout: true });
window.addEventListener("scroll",function(){
  const rootElement = document.getElementById("test");
  if(rootElement){
    document.getElementById("test").style.display = "block"; 
  }
  else {
    document.getElementById("test").style.display = "none"; 
  }
},true);
class App extends Component {
render() {
  return (
<div>
<Paper className="Paper">
<Zoom left delay={50} duration={2000} ssrFadeout>
<Navbar/>
</Zoom>
<Fade top cascade delay={1000} duration={1000} ssrFadeout>
<h1 className="text1">Full Stack Developper , Easy Way !</h1>
<br />
<h2 className="design">I design and code beautifully simple things, and I love what I do.</h2>
<br />
</Fade>
<Zoom left delay={50} duration={2000} ssrFadeout>
<img src={logo} alt="not show" className="logo" />
<h2 className="design">Welcome to my Portfolio</h2>
</Zoom>
<br />
<br />
<br />
<br />
<div id = "test">
<Wobble>
<img src={hero} alt="not show" className="hero"/>
</Wobble>
</div>
 </Paper>
 <Container />
 <br />
 <Grid />
 <br />
 <Recent />
 </div>
  );
}
}

export default App;
