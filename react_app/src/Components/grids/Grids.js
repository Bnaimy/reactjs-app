import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import { Col } from 'reactstrap';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import front from '../../images/frontend.svg';
import Typography from '@material-ui/core/Typography';
import Fade from 'react-reveal/Fade';
window.addEventListener("scroll",function(){
  const rootElement = document.getElementById("grid");
  if(rootElement){
    document.getElementById("grid").style.display = "block"; 
    console.log(rootElement);
  }
  else {
    document.getElementById("grid").style.display = "none"; 
    console.log(rootElement);
  }
},false);
const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    height: 600,
    width: 300,
  },
  control: {
    padding: theme.spacing(0),
  },
}));

export default function Gridfunction() {
  const classes = useStyles();
  return (
    <div id="grid">
      <h1 className="text1">MY SKILLS</h1>
      <br />
    <Grid container className={classes.root} spacing={0}>
      <Col lg={true}>
        <Fade left delay={100}>
            <Grid>
            <Card className={classes.paper}>
          <CardContent className="CardContent">
          <img src={front} alt="not show" className="designer"/>
          <br />
            <Typography gutterBottom variant="h5" component="h2" className="cardtext">
            Front-end Developer
            </Typography>
            <Typography variant="body2" color="textSecondary" component="p" className="cardtext">
            I like to code things from scratch, and enjoy bringing ideas to life in the browser.
            </Typography>
            <br />
            <br />
            <Typography variant="subtitle1" color="primary" component="h6" className="cardtext">
            Languages I speak: <br />
            <Typography variant="overline" color="secondary">Reactjs,Bootstrap4,CSS,Material-UI</Typography>
            </Typography>
            <br />
            <br />
            <Typography variant="subtitle1" color="primary" className="cardtext">Dev Tools: <br /></Typography>
            <Typography variant="subtitle2" color="textSecondary" className="cardtext">Gitlab<br />Github<br />Visual Studio<br />Bootstrap<br />Terminal<br /></Typography>
          </CardContent>
      </Card>
            </Grid>
            </Fade>
            </Col>
            <Col lg={true}>
        <Fade left delay={100}>
            <Grid>
            <Card className={classes.paper}>
          <CardContent className="CardContent">
          <img src={front} alt="not show" className="designer"/>
          <br />
            <Typography gutterBottom variant="h5" component="h2" className="cardtext">
            Front-end Developer
            </Typography>
            <Typography variant="body2" color="textSecondary" component="p" className="cardtext">
            I like to code things from scratch, and enjoy bringing ideas to life in the browser.
            </Typography>
            <br />
            <br />
            <Typography variant="subtitle1" color="primary" component="h6" className="cardtext">
            Languages I speak: <br />
            <Typography variant="overline" color="secondary">Reactjs,Bootstrap4,CSS,Material-UI</Typography>
            </Typography>
            <br />
            <br />
            <Typography variant="subtitle1" color="primary" className="cardtext">Dev Tools: <br /></Typography>
            <Typography variant="subtitle2" color="textSecondary" className="cardtext">Gitlab<br />Github<br />Visual Studio<br />Bootstrap<br />Terminal<br /></Typography>
          </CardContent>
      </Card>
            </Grid>
            </Fade>
            </Col>
            <Col lg={true}>
        <Fade left delay={100}>
            <Grid>
            <Card className={classes.paper}>
          <CardContent className="CardContent">
          <img src={front} alt="not show" className="designer"/>
          <br />
            <Typography gutterBottom variant="h5" component="h2" className="cardtext">
            Front-end Developer
            </Typography>
            <Typography variant="body2" color="textSecondary" component="p" className="cardtext">
            I like to code things from scratch, and enjoy bringing ideas to life in the browser.
            </Typography>
            <br />
            <br />
            <Typography variant="subtitle1" color="primary" component="h6" className="cardtext">
            Languages I speak: <br />
            <Typography variant="overline" color="secondary">Reactjs,Bootstrap4,CSS,Material-UI</Typography>
            </Typography>
            <br />
            <br />
            <Typography variant="subtitle1" color="primary" className="cardtext">Dev Tools: <br /></Typography>
            <Typography variant="subtitle2" color="textSecondary" className="cardtext">Gitlab<br />Github<br />Visual Studio<br />Bootstrap<br />Terminal<br /></Typography>
          </CardContent>
      </Card>
            </Grid>
            </Fade>
            </Col>
            </Grid>
      </div>
  )
          }
