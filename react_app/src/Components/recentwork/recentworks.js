    import React from 'react';
    import Grid from '@material-ui/core/Grid';
    import { Col , Row } from 'reactstrap';
    import { makeStyles } from '@material-ui/core/styles';
    import Fade from 'react-reveal/Fade';
    import Card from '@material-ui/core/Card';
    import CardActionArea from '@material-ui/core/CardActionArea';
    import CardActions from '@material-ui/core/CardActions';
    import CardContent from '@material-ui/core/CardContent';
    import CardMedia from '@material-ui/core/CardMedia';
    import Button from '@material-ui/core/Button';
    import Typography from '@material-ui/core/Typography';
    import hero from '../../images/java1.png';
    const email = "bouchaibnaimy1999@gmail.com" ;
    const add = "#";
    
    const useStyles = makeStyles({
      card: {
        maxWidth: 345,
      },
      media: {
        height: 300,
        paddingTop: '56.25%', // 16:9,
        marginTop:'30',
        width:'auto',
      },
      root: {
        flexGrow: 1,
      }
    });
    
    export default function MediaCard() {
      const classes = useStyles();
    
      return (
        <div>
        <Typography variant="h4" color="textPrimary" component="h4" className="cardtext">
        My Recent Work
        </Typography>
        <br />
        <Typography variant="h6" color="textSecondary" component="h6" className="cardtext">
        Here are a few recent design projects. Want to see more? <a href={"mailto:" + email}>Email me</a>
        </Typography>
        <br />
        <br />
        <br />
        <br />
      <div className={classes.root}>
      <Grid container spacing={3}>
        <Row>
        <Col lg={true}>
        <Fade left>
        <Grid item>
        <Card className={classes.card}>
          <CardActionArea>
          <CardMedia
          style={{ display:'flex', justifyContent:'center',alignSelf: 'center'}}
          className={classes.media}
          image={hero}
          title="not showing" />
            <CardContent>
              <Typography gutterBottom variant="h5" component="h2">
                JAVA WEB PROJECT
              </Typography>
              <br />
              <br />
              <br />
              <br />
              <Typography variant="body2" color="textSecondary" component="p">
                Management Absence of Student Project Developed With Spring Boot FrameWork And Mysql
              </Typography>
            </CardContent>
          </CardActionArea>
          <CardActions>
            <Button size="small" color="primary" href={add}>
              GitLAB
            </Button>
            <Button size="small" color="primary" href={add}>
              DEMO
            </Button>
          </CardActions>
        </Card>
        </Grid>
        </Fade>
        </Col>
        <Col lg={true}>
        <Fade left>
        <Grid item>
        <Card className={classes.card}>
          <CardActionArea>
          <CardMedia
          style={{ display:'flex', justifyContent:'center',alignSelf: 'center'}}
          className={classes.media}
          image={hero}
          title="not showing" />
            <CardContent>
              <Typography gutterBottom variant="h5" component="h2">
                JAVA WEB PROJECT
              </Typography>
              <br />
              <br />
              <br />
              <br />
              <Typography variant="body2" color="textSecondary" component="p">
                Management Absence of Student Project Developed With Spring Boot FrameWork And Mysql
              </Typography>
            </CardContent>
          </CardActionArea>
          <CardActions>
            <Button size="small" color="primary" href={add}>
              GitLAB
            </Button>
            <Button size="small" color="primary" href={add}>
              DEMO
            </Button>
          </CardActions>
        </Card>
        </Grid>
        </Fade>
        </Col>
        <Col lg={true}>
        <Fade left>
        <Grid item>
        <Card className={classes.card}>
          <CardActionArea>
          <CardMedia
          style={{ display:'flex', justifyContent:'center',alignSelf: 'center'}}
          className={classes.media}
          image={hero}
          title="not showing" />
            <CardContent>
              <Typography gutterBottom variant="h5" component="h2">
                JAVA WEB PROJECT
              </Typography>
              <br />
              <br />
              <br />
              <br />
              <Typography variant="body2" color="textSecondary" component="p">
                Management Absence of Student Project Developed With Spring Boot FrameWork And Mysql
              </Typography>
            </CardContent>
          </CardActionArea>
          <CardActions>
            <Button size="small" color="primary" href={add}>
              GitLAB
            </Button>
            <Button size="small" color="primary" href={add}>
              DEMO
            </Button>
          </CardActions>
        </Card>
        </Grid>
        </Fade>
        </Col>
        <Col lg={true}>
        <Fade left>
        <Grid item>
        <Card className={classes.card}>
          <CardActionArea>
          <CardMedia
          style={{ display:'flex', justifyContent:'center',alignSelf: 'center'}}
          className={classes.media}
          image={hero}
          title="not showing" />
            <CardContent>
              <Typography gutterBottom variant="h5" component="h2">
                JAVA WEB PROJECT
              </Typography>
              <br />
              <br />
              <br />
              <br />
              <Typography variant="body2" color="textSecondary" component="p">
                Management Absence of Student Project Developed With Spring Boot FrameWork And Mysql
              </Typography>
            </CardContent>
          </CardActionArea>
          <CardActions>
            <Button size="small" color="primary" href={add}>
              GitLAB
            </Button>
            <Button size="small" color="primary" href={add}>
              DEMO
            </Button>
          </CardActions>
        </Card>
        </Grid>
        </Fade>
        </Col>
        </Row>
        </Grid>
        </div>
        </div>
      );
    }
    