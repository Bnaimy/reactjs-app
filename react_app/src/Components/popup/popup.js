import React , { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';
import TextField from '@material-ui/core/TextField';
import useForm from "react-hook-form";
import axios from 'axios';
import swal from 'sweetalert';
const useStyles = makeStyles(theme => ({
  appBar: {
    position: 'relative',
  },
  title: {
    marginLeft: theme.spacing(2),
    flex: 1,
  },
}));
const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});
export default function FullScreenDialog() {
const [name, setname] = useState( '' );
const [email, setemail] = useState( '' );
const [message, setMessage] = useState( '' );
let data = {name:name,email:email,message:message};
  const handleSubmit1 = (event)=>{
    event.preventDefault();
    const url = "http://localhost:8080/api/sendhi";
    axios.post(url,data)
          .then(res=>{
            swal({
              title: "Message sent with success!",
              text: "I Will Reply Soon !",
              icon: "success",
              button: "Cancel",
            });
          }
          )
          .catch(error => {
            swal({
              title: "Oops...",
              text: "Something went wrong!",
              icon: "error",
              button: "Cancel",
            });
          })
        }
  useForm();
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);
  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
<Button variant="outlined" color="primary" className="b1" onClick={handleClickOpen}>say Hi</Button>
      <Dialog fullScreen open={open} onClose={handleClose} TransitionComponent={Transition}>
        <AppBar className={classes.appBar}>
          <Toolbar>
            <IconButton edge="start" color="inherit" onClick={handleClose} aria-label="close">
              <CloseIcon />
            </IconButton>
          </Toolbar>
        </AppBar>
        <form onSubmit={handleSubmit1}>
  <TextField
        name="name"
        id="name"
        label="Name"
        margin="normal"
        variant="outlined"
        fullWidth
        required
        inputProps={{ pattern: "[a-zA-Z]+" }}
        onChange={e => setname(e.target.value)}
  />
  <TextField
        name="email"
        id="email"
        label="EMAIL"
        margin="normal"
        variant="outlined"
        fullWidth
        required
        inputProps={{ pattern: "[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,}$" }}
        onChange={e => setemail(e.target.value)}
  />
<TextField
  name="message"
  id="message"
  margin="normal"
  variant="outlined"
  label="MESSAGE"
  fullWidth
  multiline={true}
  rows={10}
  rowsMax={20}
  required
  onChange={e => setMessage(e.target.value)}
/>
<Button variant="outlined" color="primary" type='submit'>
SAY HI
</Button>
  </form>
      </Dialog>
    </div>
  );
}